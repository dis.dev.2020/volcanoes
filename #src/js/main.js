'use strict';

$(function() {

    let isProjectMap         = $('div').is('.project__map');
    let isProjectContent     = $('div').is('.project');
    let isContractor         = $('div').is('.contractor');

    // SVG IE11 support
    svg4everybody();

   /*     
    $('[data-fancybox="gallery"]').fancybox({
        thumbs : {
            autoStart : true,
            hideOnClose : true,  
        },
        buttons: [
            "zoom",
            "slideShow",
            "fullScreen",
            "close"
          ],
    });
*/
    // nav
    $('.nav_toggle').on('click', function(e){
        e.preventDefault();
        $('.page').toggleClass('nav_open');
    });

    // lng
    $('.lng').on('click', function(e){
        e.preventDefault();
        $('.lng').toggleClass('lng_switch');
    });

    //   place
    let header = $('.header');
    let $h = header.offset().top;
    let hPage = $('.header__container').offset().top;

    if (hPage > 23) {
        $('.page').addClass('fix_top');
    }

    console.log(hPage);

    $(window).scroll(function () {
        if ($(window).scrollTop() > $h) {
            $('.page').addClass('fix_top');
        } else {
            $('.page').removeClass('fix_top');
        }
    });

    // Scroll
    $('.btn_scroll').on('click', function(){
        var str=$(this).attr('href');
        $.scrollTo(str, {
            duration: 500,
            offset: - 90
        });
        return false;
    });

    $('.btn_nav').on('click', function(){
        $('.page').removeClass('nav_open');
        var str=$(this).attr('href');
        $.scrollTo(str, {
            duration: 500,
            offset: -90
        });
        return false;
    });


    // Sticky
    if (isProjectMap) {
        let winWidth =$(window).width();
        let winHeight =$(window).height();
        let offset = 0;
    
        if (winHeight < 800 ) {
            offset = 20
        }
        else  {
            offset = 50;
        }
    
        if (winWidth > 766) {
    
            let layout = $('.project__map_inner');
            let $h = layout.offset().top;
            $(window).scroll(function () {
                if ($(window).scrollTop() > $h) {
                    layout.addClass('fix');
                } else {
                    layout.removeClass('fix');
                }
            });
        }
    }

    $('.place').on('click',function(){
        let check = $(this).hasClass('hover');
        if (check) {
            $(this).removeClass('hover');
        }
        else {
            $('.place').removeClass('hover');
            $(this).addClass('hover');
        }
    });

    $(".chart__section")
        .mouseenter(function() {
            $('.chart__section').removeClass('active');
            $('.chart__box').removeClass('active');
            $(this).addClass('active');
            let box = $('.chart__box_' + $(this).attr('data-target'));
            box.addClass('active');
        })
        .mouseleave(function(){
            $('.chart__section').removeClass('active');
            $('.chart__box').removeClass('active');
        });


    $(".chart__center")
        .mouseenter(function() {
            $('.chart__section').addClass('active');
        })
        .mouseleave(function(){
            $('.chart__section').removeClass('active');
        });

    let press = new Swiper('.press__slider', {
        loop: false,
        slidesPerView: 'auto',
        spaceBetween: 8,
        navigation: {
            nextEl: '.press__nav_next',
            prevEl: '.press__nav_prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 15,
            },
            1600: {
                spaceBetween: 30,
            }
        }
    });

    let partners = new Swiper('.partners__slider', {
        loop: false,
        slidesPerView: 'auto',
        spaceBetween: 8,
        navigation: {
            nextEl: '.partners__nav_next',
            prevEl: '.partners__nav_prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 15,
            },
            1600: {
                spaceBetween: 30,
            }
        }
    });

    let presentation = new Swiper('.presentation__slider', {
        loop: true,
        slidesPerView: 'auto',
        spaceBetween: 6,
        navigation: {
            nextEl: '.presentation__nav_next',
            prevEl: '.presentation__nav_prev',
        },
        breakpoints: {
            370: {
                spaceBetween: 15,
            },
            1600: {
                spaceBetween: 20,
            }
        }
    })

    // Project

    if (isProjectContent) {

        let step_start = false;
        function scrollTrackingStart(){
            if (step_start) {
                return false;
            }
    
            var wt = $(window).scrollTop();
            var wh = $(window).height();
            var et = $('.project__content_start').offset().top;
            var eh = $('.project__content_start').outerHeight();
            var dh = $(document).height();
    
            if (wt + wh >= et || wh + wt == dh || eh + et < wh){
                step_start = true;
    
               $('.map').addClass('active_step_start')
            }
        }
    
        let step_one = false;
        function scrollTrackingOne(){
            if (step_one) {
                return false;
            }
    
            var wt = $(window).scrollTop();
            var wh = $(window).height();
            var et = $('.project__content_one').offset().top;
            var eh = $('.project__content_one').outerHeight();
            var dh = $(document).height();
    
            if (wt + wh >= et || wh + wt == dh || eh + et < wh){
                step_one = true;
    
                $('.map').addClass('active_step_start');
                $('.map').addClass('active_step_one');
            }
        }
    
        let step_two = false;
        function scrollTrackingTwo(){
            if (step_two) {
                return false;
            }
    
            var wt = $(window).scrollTop();
            var wh = $(window).height();
            var et = $('.project__content_two').offset().top;
            var eh = $('.project__content_two').outerHeight();
            var dh = $(document).height();
    
            if (wt + wh >= et || wh + wt == dh || eh + et < wh){
                step_two = true;
    
                $('.map').addClass('active_step_start');
                $('.map').addClass('active_step_one');
                $('.map').addClass('active_step_two');
            }
        }
    
        $(window).scroll(function(){
            scrollTrackingStart();
            scrollTrackingOne();
            scrollTrackingTwo();
        });
    
        $(document).ready(function(){
            scrollTrackingStart();
            scrollTrackingOne();
            scrollTrackingTwo();
        });
    }
    
    let tourismSlider = new Swiper('.tourism-slider', {
        loop: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        navigation: {
            nextEl: '.tourism-next',
            prevEl: '.tourism-prev',
        },
        breakpoints: {
            1024: {
                spaceBetween: 16,
            },
            1600: {
                spaceBetween: 20,
            }
        }
    })
    
    $('.card').on('click',function(){
        let check = $(this).hasClass('hover');
        if (check) {
            $(this).removeClass('hover');
        }
        else {
            $('.card').removeClass('hover');
            $(this).addClass('hover');
        }
    });

        
    let regionSliderSm = new Swiper('.region-slider-sm', {
        loop: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        navigation: {
            nextEl: '.region-slider-sm-next',
            prevEl: '.region-slider-sm-prev',
        },
        breakpoints: {
            1024: {
                spaceBetween: 16,
            },
            1600: {
                spaceBetween: 20,
            }
        }
    })
           
    let regionSliderMD = new Swiper('.region-slider-md', {
        loop: true,
        direction: 'vertical',
        slidesPerView: 2,
        spaceBetween: 10,
        navigation: {
            nextEl: '.region-slider-md-next',
            prevEl: '.region-slider-md-prev',
        },
        breakpoints: {
            1600: {
                spaceBetween: 16,
            }
        }
    })
            
    let managementSlider = new Swiper('.management-slider', {
        loop: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        breakpoints: {
            678: {
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 2,
                slidesPerColumn: 2,
                spaceBetween: 20,
            },
            1600: {
                slidesPerView: 2,
                slidesPerColumn: 2,
                spaceBetween: 20,
            },
        }
    })
                
    let contentMediaSlider = new Swiper('.content-media-slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 10,
        navigation: {
            nextEl: '.content__media--button.next',
            prevEl: '.content__media--button.prev',
        },
    })

    // Mobile News
    let mobileNews = new Swiper('.news-slider', {
        loop: false,
        slidesPerView: 'auto',
        spaceBetween: 8,
        navigation: {
            nextEl: '.press__nav_next',
            prevEl: '.press__nav_prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 15,
            },
            1600: {
                spaceBetween: 30,
            }
        }
    });

    let  newsDesktop = new Swiper('.news-slider-desktop', {
        slidesPerView: 3,
        slidesPerColumn: 2,
        spaceBetween: 24,
        simulateTouch: false,
        breakpoints: {
            1330: {
                slidesPerView: 4,
                slidesPerColumn: 2,
                spaceBetween: 24,
            },
        }
    });
    
    let  connectSlider = new Swiper('.connect-slider', {
        slidesPerView: 1,
        spaceBetween: 1,
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 6,
            },
            1024: {
                slidesPerView: 4,
                spaceBetween: 6,
            },
            1330: {
                slidesPerView: 4,
                spaceBetween: 32,
            },
            1440: {
                slidesPerView: 4,
                spaceBetween: 68,
            },
          }
    });
        
    let contractorQuote = new Swiper('.contractor-quote-slider', {
        loop: false,
        speed: 800,
        slidesPerView: 1,
        spaceBetween: 40,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    })

    let contractorPrimary = new Swiper('.contractor-primary-slider', {
        loop: false,
        simulateTouch: false,
        parallax: true,
        speed: 800,
        slidesPerView: 1,
        spaceBetween: 20,
        autoplay: {
            delay: 15000000,
          },
        pagination: {
            el: '.contractor__nav',
            clickable: true
        },
    })

    contractorPrimary.on('slideChange', function () {
        let sld = contractorPrimary.realIndex;
        console.log(sld);
        contractorQuote.slideTo(sld, 600);
    });

    if (isContractor) {
        let $contractorHeight = $('.contractor__data').offset().top;
        let contractorVisible = false;
        
        $(window).on('scroll', function () {
            let $w = $(window).scrollTop() + ($(window).height() / 2);
    
            if (!contractorVisible) {
    
                if ($w > $contractorHeight) {
    
                    contractorVisible = true;
    
                    let num1 = 0,
                        num2 = 0,
                        num3 = 0,
                        result1 = $('.contractor-value-1').attr('data-value'),
                        result2 = $('.contractor-value-2').attr('data-value'),
                        result3 = $('.contractor-value-3').attr('data-value');
    
                    console.log(result1);
                    console.log(result2);
                    console.log(result3);
    
                    setInterval(function () {
                        num1 = num1 + 1;
                        if (num1 <= result1) {
                            $('.contractor-value-1').text(num1);
                        };
                    }, 10);
        
                    setInterval(function () {
                        num2 = num2 + 1;
                        if (num2 <= result2) {
                            $('.contractor-value-2').text(num2);
                        };
                    }, 30);
        
                    setInterval(function () {
                        num3 = num3 + 50;
                        if (num3 <= result3) {
                            $('.contractor-value-3').text(num3);
                        };
                    }, 200);
                    
                    $('.contractor__data').addClass('block-visible');
                }
            }
        });
    }


    // Project Map

    $('.map-point').on('click', function(){
        
        let infobox = $(this).attr('data-target');
        console.log(infobox);

        $('.infobox').removeClass('visible');
        $('.infobox').addClass('visible');
    });

    $('.infobox').on('click', function(){
        $('.infobox').removeClass('visible');
    });
    

    $('.feature__header').on('click', function(){
        let featureItem = $($(this).closest('.feature'));
        if (featureItem.hasClass('open')) {
            featureItem.removeClass('open');
            featureItem.find('.feature__content').slideUp('fast');
        }
        else {
            $('.feature').removeClass('open');
            $('.feature__content').slideUp('fast');
            featureItem.addClass('open');
            featureItem.find('.feature__content').slideDown('fast');
        }
    });

    let galleryThumbs = new Swiper('.gallery-thumbs', {
        loop: false,
        slidesPerView: 'auto',
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        spaceBetween: 10,
        breakpoints: {
            768: {
                spaceBetween: 7,
                slidesPerView: 'auto',
            },
            1600: {
                spaceBetween: 9,
                slidesPerView: 6,
            }
        }
    });

    let galleryMain = new Swiper('.gallery-main', {
        spaceBetween: 10,
        navigation: {
          nextEl: '.gallery-next',
          prevEl: '.gallery-prev',
        },
        thumbs: {
          swiper: galleryThumbs
        }
    });

    $('.gallery-toggle').on('click', function(e){
        e.preventDefault();
        $('.gallery').toggleClass('open');
    });

    // Project chart 
    /*
    let isEvo = $('div').is('.evo');
    if (isEvo) {
        let winWidth =$(window).width();
        let winHeight =$(window).height();
        let offset = 0;
    
        if (winWidth > 767) {
    
            let layout = $('.evo__media');
            let $h = layout.offset().top;
            
            console.log(window.screen.availHeight);
            let layoutNext = $('.management'); 
            let $hNext = layoutNext.offset().top - window.screen.availHeight + 120;

            $(window).scroll(function () {
     
                if ($(window).scrollTop() > $h) {
                    layout.addClass('fix');
                } else {
                    layout.removeClass('fix');
                }

                if ($(window).scrollTop() > $hNext) {
                    layout.removeClass('fix');
                    layout.addClass('fix-bottom');
                } 
                else {
                    layout.removeClass('fix-bottom');
                }
            });

        }
    }
    */

   $('.evo__media').stick_in_parent({
        offset_top: 20
    });


    let preamble = new Swiper('.preamble-slider', {
        loop: true,
        spaceBetween: 20,
        pagination: {
            el: '.preamble__pagination',
            clickable: true
        },
        autoplay: {
            delay: 15000,
            disableOnInteraction: true,
        },
    })

    let companions = new Swiper('.companions-slider', {
        loop: false,
        speed: 600,
        simulateTouch: false,
        slidesPerView: 'auto',
        spaceBetween: 15,
        navigation: {
            nextEl: '.companions-next',
            prevEl: '.companions-prev',
        },
    });

    $('.person').on('click',function(){
        let check = $(this).hasClass('hover');
        if (check) {
            $(this).removeClass('hover');
        }
        else {
            $('.person').removeClass('hover');
            $(this).addClass('hover');
        }
    });

    $(".person__back--content").mCustomScrollbar({
        theme: 'minimal-dark'
    });


    $('.director').on('click',function(){
        let check = $(this).hasClass('hover');
        if (check) {
            $(this).removeClass('hover');
        }
        else {
            $('.person').removeClass('hover');
            $(this).addClass('hover');
        }
    });


    $(".director__back--content").mCustomScrollbar({
        theme: 'minimal-dark'
    });

    // Sub nav
    $('.nav-second-open').on('click', function(e){
        e.preventDefault();
        let nav = $($(this).attr('href'));
        $('.subnav').removeClass('active');
        nav.addClass('active');
        $('.page').addClass('subnav-open');
    });

    $('.nav-second__layout').on('click', function(e){
        e.preventDefault();
        $('.page').removeClass('subnav-open');
    });


    // Hide nav
    $('body').click(function (event) {
        if ($(event.target).closest(".nav-second-open").length === 0) {

            if ($(event.target).closest(".nav-second").length === 0) {
                $('.subnav').removeClass('active');
                $(".page").removeClass('subnav-open');
            }
        }
    });
});

